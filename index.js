// parent class
class Vehicle {
    constructor(type, manufacturer) {
        this.type = type;
        this.manufacturer = manufacturer;
    }

    info() {
        console.log(`Jenis kendaraan roda ${this.type} dari negara ${this.manufacturer}`);
    }
}

// child class from class Vehicle
class Car extends Vehicle {
    constructor(type, manufacturer, brand, price, tax) {
        super(type, manufacturer);

        this.brand = brand;
        this.price = price;
        this.tax = tax;
    }

    totalPrice() {
        let finalPrice = this.price + (this.tax / 100 * this.price);

        return finalPrice;
    }

    //overriding method info
    info() {
        super.info();
        console.log(`Kendaraan ini mereknya ${this.brand} dengan total harga ${this.totalPrice()}`);
    }
}

// instance of class Vehicle
let vehicle = new Vehicle(2, 'Konoha');
vehicle.info();

console.log('-------------------------');

// instance of class Car
let car = new Car(4, 'Asgard', 'Tesla', 800000000, 10);
car.info();